#include <iostream>
#include <cmath>
#include "scipy_optimize.h"

#define CATCH_CONFIG_MAIN
#include "catch.hpp"


class SimpleCostFunction : public scipy_optimize::CostFunction {
private:
    double A, B, C, D, E;
public:
    double startx[2] = {0, 0};
    double answer[2] = {-0.25, -0.3};

    SimpleCostFunction(double A, double B, double C, double D, double E){
        this->A = A;
        this->B = B;
        this->C = C;
        this->D = D;
        this->E = E;
    }
    double operator()(const Eigen::VectorXd &x, Eigen::VectorXd &gradient, bool need_gradient=true) override final {
        num_calls++;
        double cost = A + B*x[0] + C*x[1] + D*x[0]*x[0] + E*x[1]*x[1];
        if (need_gradient)
            scipy_optimize::compute_gradient(*this, x, gradient, cost);
        return cost;
    }
};


TEST_CASE("Simple cost cunction") {
    auto cf = SimpleCostFunction(1, 2, 3, 4, 5);
    Eigen::VectorXd x0(2);
    x0 << cf.startx[0], cf.startx[1];

    SECTION("Nelder-Mead minimization") {
        const auto res = scipy_optimize::fmin_neldermead(cf, x0);
        INFO(res);

        // Compare with results from scipy Python code
        REQUIRE(res.error_code == scipy_optimize::MinimizationStatus::OK);
        REQUIRE(res.final_x[0] == Approx(cf.answer[0]).epsilon(1e-4));
        REQUIRE(res.final_x[1] == Approx(cf.answer[1]).epsilon(1e-4));
    }

    SECTION("BFGS minimization") {
        const auto res = scipy_optimize::fmin_bfgs(cf, x0);
        INFO(res);

        // Compare with results from scipy Python code
        REQUIRE(res.error_code == scipy_optimize::MinimizationStatus::OK);
        REQUIRE(res.final_x[0] == Approx(cf.answer[0]).epsilon(1e-8));
        REQUIRE(res.final_x[1] == Approx(cf.answer[1]).epsilon(1e-8));
    }

    SECTION("auto minimization") {
        const auto res = scipy_optimize::minimize(cf, x0); // should use BFGS
        INFO(res);

        // Compare with results from scipy Python code
        REQUIRE(res.error_code == scipy_optimize::MinimizationStatus::OK);
        REQUIRE(res.final_x[0] == Approx(cf.answer[0]).epsilon(1e-8));
        REQUIRE(res.final_x[1] == Approx(cf.answer[1]).epsilon(1e-8));
    }
}


class HimmelblauCostFunction : public scipy_optimize::CostFunction {
public:
    double startx[2] = {-0.2, 0.0};
    double answer[2] = {3.0, 2.0};

    double operator()(const Eigen::VectorXd &x, Eigen::VectorXd &gradient, bool need_gradient=true) override final {
        num_calls++;
        double a = x[0]*x[0] + x[1] - 11;
        double b = x[0] + x[1]*x[1] - 7;
        double cost = a*a + b*b;

        if (need_gradient) {
            gradient[0] = 4*pow(x[0], 3) + 4*x[0]*x[1] - 42*x[0] + 2*x[1]*x[1] - 14;
            gradient[1] = 2*pow(x[0], 2) + 4*x[0]*x[1] + 4*pow(x[1], 3) - 26*x[1] - 22;
        }
        return cost;
    }
};

TEST_CASE("Himmelblau cost cunction") {
    auto cf = HimmelblauCostFunction();
    Eigen::VectorXd x0(2);
    x0 << cf.startx[0], cf.startx[1];

    SECTION("Nelder-Mead minimization") {
        const auto res = scipy_optimize::fmin_neldermead(cf, x0);
        INFO(res);

        // Compare with results from scipy Python code
        //REQUIRE(res.error_code == scipy_optimize::MinimizationStatus::OK);
        REQUIRE(res.final_x[0] == Approx(cf.answer[0]).epsilon(1e-4));
        REQUIRE(res.final_x[1] == Approx(cf.answer[1]).epsilon(1e-4));
    }

    SECTION("BFGS minimization") {
        const auto res = scipy_optimize::fmin_bfgs(cf, x0);
        INFO(res);

        // Compare with results from scipy Python code
        //REQUIRE(res.error_code == scipy_optimize::MinimizationStatus::OK);
        REQUIRE(res.final_x[0] == Approx(cf.answer[0]).epsilon(1e-7));
        REQUIRE(res.final_x[1] == Approx(cf.answer[1]).epsilon(1e-7));
    }
}


class RosenbrockCostFunction : public scipy_optimize::CostFunction {
public:
    double startx[3] = {0.8, 1.2, 0.7};
    double answer[3] = {1.0, 1.0, 1.0};

    double operator()(const Eigen::VectorXd &x, Eigen::VectorXd &gradient, bool need_gradient=true) override final {
        num_calls++;
        double cost = 100.0 * pow(x[1] - x[0]*x[0], 2) + pow(1 - x[0], 2);
        cost += 100.0 * pow(x[2] - x[1]*x[1], 2) + pow(1 - x[1], 2);

        if (need_gradient) {
            gradient[0] = -400 * x[0] * (x[1] - x[0]*x[0]) - 2 * (1 - x[0]);
            gradient[1] = (200 * (x[1] - x[0]*x[0]) - 400 * (x[2] - x[1]*x[1]) * x[1] - 2 * (1 - x[1]));
            gradient[2] = 200 * (x[2] - x[1]*x[1]);
        }
        return cost;
    }
};

TEST_CASE("Rosenbrock cost cunction") {
    auto cf = RosenbrockCostFunction();
    Eigen::VectorXd x0(3);
    x0 << cf.startx[0], cf.startx[1], cf.startx[2];

    SECTION("Nelder-Mead minimization") {
        const auto res = scipy_optimize::fmin_neldermead(cf, x0);
        INFO(res);

        // Compare with results from scipy Python code
        REQUIRE(res.error_code == scipy_optimize::MinimizationStatus::OK);
        REQUIRE(res.final_x[0] == Approx(cf.answer[0]).epsilon(1e-5));
        REQUIRE(res.final_x[1] == Approx(cf.answer[1]).epsilon(1e-5));
        REQUIRE(res.final_x[2] == Approx(cf.answer[2]).epsilon(1e-5));
    }

    SECTION("BFGS minimization") {
        const auto res = scipy_optimize::fmin_bfgs(cf, x0);
        INFO(res);

        // Compare with results from scipy Python code
        REQUIRE(res.error_code == scipy_optimize::MinimizationStatus::OK);
        REQUIRE(res.final_x[0] == Approx(cf.answer[0]).epsilon(1e-8));
        REQUIRE(res.final_x[1] == Approx(cf.answer[1]).epsilon(1e-8));
        REQUIRE(res.final_x[2] == Approx(cf.answer[2]).epsilon(1e-8));
    }
}
