# encoding: utf-8
from __future__ import division
import scipy.optimize.optimize as opt
import numpy as np


###############################################################################
print 'TEST: simple_cost_function'


def simple_cost_function(x):
    A, B, C, D, E = 1, 2, 3, 4, 5
    return A + B*x[0] + C*x[1] + D*x[0]*x[0] + E*x[1]*x[1]


def printres(name, res, verbose=True):
    print '  %s: %r' % (name, tuple(res['x']))
    if verbose:
        print '    ' + '\n    '.join(line.strip() for line in str(res).split('\n'))


res = opt._minimize_neldermead(simple_cost_function, [0, 0], disp=False)
printres('Nelder-Mead', res)
res = opt._minimize_bfgs(simple_cost_function, [0, 0], disp=False)
printres('BFGS', res)


###############################################################################
print 'TEST: himmelblau'


def himmelblau(p):
    """
    R^2 -> R^1 test function for optimization.  The function has four local
    minima where himmelblau(xopt) == 0.
    """
    x, y = p
    a = x*x + y - 11
    b = x + y*y - 7
    return a*a + b*b


def himmelblau_grad(p):
    x, y = p
    return np.array([4*x**3 + 4*x*y - 42*x + 2*y**2 - 14,
                     2*x**2 + 4*x*y + 4*y**3 - 26*y - 22])


x0 = [-0.2, 0.0]
res = opt._minimize_neldermead(himmelblau, x0, disp=False)
printres('Nelder-Mead', res)
res = opt._minimize_bfgs(himmelblau, x0, disp=False, jac=himmelblau_grad)
printres('BFGS', res)
