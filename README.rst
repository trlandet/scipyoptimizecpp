ScipyOptimizeCpp
================

A quick and dirty port of parts of scipy.optimize to C++. The Nelder-Mead and
BFGS solvers are included. For documentation and references to the methods see
the original Python code at http://scipy.org/scipylib.

This library is header only, you only need one file, ``scipy_optimize.h``,
which depends on Eigen (http://eigen.tuxfamily.org).

The unit tests, ``test.cpp`` serve as testing and example of usage. These
depend on Catch (https://github.com/philsquared/Catch) which is bundled
in ``catch.hpp``. A quick example from there is shown below:

::

    class RosenbrockCostFunction : public scipy_optimize::CostFunction {
    public:
        double operator()(const Eigen::VectorXd &x, Eigen::VectorXd &gradient, bool need_gradient=true) override final {
            num_calls++;
            double cost = 100.0 * pow(x[1] - x[0]*x[0], 2) + pow(1 - x[0], 2);
            cost += 100.0 * pow(x[2] - x[1]*x[1], 2) + pow(1 - x[1], 2);

            if (need_gradient) {
                gradient[0] = -400 * x[0] * (x[1] - x[0]*x[0]) - 2 * (1 - x[0]);
                gradient[1] = (200 * (x[1] - x[0]*x[0]) - 400 * (x[2] - x[1]*x[1]) * x[1] - 2 * (1 - x[1]));
                gradient[2] = 200 * (x[2] - x[1]*x[1]);
            }
            return cost;
        }
    };

    void solve() {
        auto cf = RosenbrockCostFunction();
        Eigen::VectorXd x0(3);
        x0 << 0.8, 1.2, 0.7;

        // Solve and print results
        const auto res = scipy_optimize::fmin_neldermead(cf, x0);
        std::cout << res << std::endl;
    }


This code should **not be trusted for any purpose** without carefull testing!
Even thought there are some tests included it is quite likely that errors were
introduced in the porting process.

The license of the code is the SciPy 3-clause new BSD license as the code is of
course highly derivatory as it is a straight port of the SciPy code.


Tormod Landet, 2017
