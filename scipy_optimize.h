/*
 * Port by Tormod Landet of parts of scipy.optimize to C++ based
 * on the latest git commit f80aef7 from 18 Dec 2016.
 * Ported in February of 2017
 *

 *****************************************************************************
 ** SciPy has the following license
 *****************************************************************************

Copyright (c) 2001, 2002 Enthought, Inc.
All rights reserved.

Copyright (c) 2003-2016 SciPy Developers.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

  a. Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
  b. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
  c. Neither the name of Enthought nor the names of the SciPy Developers
     may be used to endorse or promote products derived from this software
     without specific prior written permission.


THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
THE POSSIBILITY OF SUCH DAMAGE.

 *****************************************************************************
 ** The original Python file, optimize.py, had the following header:
 *****************************************************************************

# ******NOTICE***************
# optimize.py module by Travis E. Oliphant
#
# You may copy and use this module as you see fit with no
# guarantee implied provided you keep this notice in all copies.
# *****END NOTICE************

# A collection of optimization algorithms.  Version 0.5
# CHANGES
#  Added fminbound (July 2001)
#  Added brute (Aug. 2002)
#  Finished line search satisfying strong Wolfe conditions (Mar. 2004)
#  Updated strong Wolfe conditions line search to use
#      cubic-interpolation (Mar. 2004)

*/
#ifndef SCIPY_OPTIMIZE_H
#define SCIPY_OPTIMIZE_H

#include <iostream>
#include <string>
#include <vector>
#include <tuple>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <type_traits>
#include <Eigen/Dense>

namespace scipy_optimize {


/*
    Cost function taking a vector coordinate and (output parameter) vector gradient
    and returning the scalar cost
*/
class CostFunction {
public:
    std::size_t num_calls = 0;
    virtual double operator()(const Eigen::VectorXd &x, Eigen::VectorXd &gradient, bool need_gradient=true) = 0;
};


/*
    Cost function taking a scalar coordinate and returning (cost, gradient)
*/
class ScalarCostFunction {
public:
    std::size_t num_calls = 0;
    virtual std::tuple<double, double> operator()(const double s, bool need_gradient=true) = 0;
};


const double SCIPY_EPSILON = 1.4901161193847656e-08;
void compute_gradient(CostFunction &cost_function, const Eigen::VectorXd &x0, Eigen::VectorXd &gradient,
                      const double x0_cost, const double eps=SCIPY_EPSILON) {
    std::size_t N = x0.rows();
    Eigen::VectorXd x(x0);
    for (std::size_t i = 0; i < N; i++) {
        x[i] = x0[i] + eps;
        const double x_cost = cost_function(x, gradient, false);
        gradient[i] = (x_cost - x0_cost)/eps;
        x[i] = x0[i];
    }
}


std::vector<std::size_t> argsort(const Eigen::VectorXd &v) {
  // initialize original index locations
  std::vector<std::size_t> idx(v.rows());
  std::iota(idx.begin(), idx.end(), 0);

  // sort indexes based on comparing values in v
  std::sort(idx.begin(), idx.end(),
            [&v](size_t i1, size_t i2) {return v[i1] < v[i2];});

  return idx;
}


enum MinimizationStatus {
    OK = 0,
    TOO_MANY_COST_CUNCTION_CALLS = 1,
    TOO_MANY_ITERATIONS = 2,
    LINE_SEARCH_FAILED_TO_IMPROVE_SOLUTION = 3,
    GOT_NAN_COST = 4
};
std::ostream &operator<<(std::ostream &os, const MinimizationStatus &status) {
    if (status == MinimizationStatus::OK)
        return os << "0 (OK)";
    else if (status == MinimizationStatus::TOO_MANY_COST_CUNCTION_CALLS)
        return os << (int) status << " (TOO_MANY_COST_CUNCTION_CALLS)";
    else if (status == MinimizationStatus::TOO_MANY_ITERATIONS)
        return os << (int) status << " (TOO_MANY_ITERATIONS)";
    else if (status == MinimizationStatus::LINE_SEARCH_FAILED_TO_IMPROVE_SOLUTION)
        return os << (int) status << " (LINE_SEARCH_FAILED_TO_IMPROVE_SOLUTION)";
    else if (status == MinimizationStatus::GOT_NAN_COST)
        return os << (int) status << " (GOT_NAN_COST)";
    else
        return os << (int) status << " (UNKNOWN STATUS)";
}


struct MinimizationResult {
    std::size_t num_cost_function_calls = 0;
    std::size_t num_iterations = 0;
    MinimizationStatus error_code = MinimizationStatus::OK;
    double final_cost = 0.0;
    Eigen::VectorXd final_x;
    Eigen::VectorXd final_gradient;
    double diff_x = -1;
    double diff_cost = -1;
    double gradient_norm = -1;
};
std::ostream &operator<<(std::ostream &os, const MinimizationResult &res) {
    return os << "MinimizationResult:" << std::endl
              << "  num_cost_function_calls = " << res.num_cost_function_calls << std::endl
              << "  num_iterations = " << res.num_iterations << std::endl
              << "  error_code = " << res.error_code << std::endl
              << "  diff_x = " << res.diff_x << std::endl
              << "  gradient_norm = " << res.gradient_norm << std::endl
              << "  diff_cost = " << res.diff_cost << std::endl
              << "  final_cost = " << res.final_cost << std::endl
              << "  final_x = " << res.final_x.transpose() << std::endl
              << "  final_gradient = " << res.final_gradient.transpose() << std::endl;
}


/*
    Minimization of scalar function of one or more variables using the
    Nelder-Mead algorithm.

    cost_function : callable f(x, gradient, calc_gradient)
        Objective function to be minimized.
    maxiter, maxfev : size_t, optional
        Maximum allowed number of iterations and function evaluations.
        Will default to ``N*200``, where ``N`` is the number of
        variables, if neither `maxiter` or `maxfev` is set. If both
        `maxiter` and `maxfev` are set, minimization will stop at the
        first reached.
    xatol : double, optional
        Absolute error in xopt between iterations that is acceptable for
        convergence.
    fatol : double optional
        Absolute error in func(xopt) between iterations that is acceptable for
        convergence.
*/
MinimizationResult fmin_neldermead(CostFunction &cost_function, const Eigen::VectorXd &x0,
                                   std::size_t maxiter=0, std::size_t maxfev=0,
                                   const double xatol=1e-4, const double fatol=1e-4) {
    const double rho = 1;
    const double chi = 2;
    const double psi = 0.5;
    const double sigma = 0.5;
    const double nonzdelt = 0.05;
    const double zdelt = 0.00025;
    const double inf = std::numeric_limits<double>::infinity();

    const std::size_t N = x0.rows();
    Eigen::VectorXd y(N);
    Eigen::MatrixXd sim(N + 1, N);
    sim.row(0) = x0;
    for (int k = 0; k < N; k++){
        y = x0;
        if(y[k] != 0)
            y[k] = (1 + nonzdelt)*y[k];
        else
            y[k] = zdelt;
        sim.row(k + 1) = y;
    }

    // If neither are set, then set both to default
    if (maxiter == 0 && maxfev == 0) {
        maxiter = N * 200;
        maxfev = N * 200;
    }
    else if (maxiter == 0) {
        // Convert remaining zeros, to inf, unless the other is inf, in
        // which case use the default to avoid unbounded iteration
        if (maxfev == inf)
            maxiter = N * 200;
        else
            maxiter = inf;
    }
    else if (maxfev == 0) {
        if (maxiter == inf)
            maxfev = N * 200;
        else
            maxfev = inf;
    }

    Eigen::VectorXd fsim(N + 1);
    for (int k = 0; k < N + 1; k++) {
        y = sim.row(k);
        fsim[k] = cost_function(y, y, false);
    }

    // Sort by increasing fsim value
    std::vector<std::size_t> ind = argsort(fsim);
    Eigen::VectorXd fsim_sorted(N + 1);
    Eigen::MatrixXd sim_sorted(N + 1, N);
    for (int k = 0; k < N + 1; k++) {
        sim_sorted.row(k) = sim.row(ind[k]);
        fsim_sorted[k] = fsim[ind[k]];
    }
    sim = sim_sorted;
    fsim = fsim_sorted;


    std::size_t iterations = 1;
    double max_diff_x, max_diff_f;
    while (cost_function.num_calls < maxfev && iterations < maxiter) {
        // Check convergence in xatol and fatol
        max_diff_x = 0; max_diff_f = 0;
        for(int k = 1; k < N + 1; k++) {
            for(int i = 0; i < N; i++)
                max_diff_x = std::max(max_diff_x, std::abs(sim(k, i) - sim(0, i)));
            max_diff_f = std::max(max_diff_f, std::abs(fsim(k) - fsim(0)));
        }
        if (max_diff_x <= xatol && max_diff_f <= fatol) break;

        Eigen::VectorXd xbar(N);
        for (int i = 0; i < N; i++) {
            xbar(i) = sim.block(0, i, N, 1).sum() / N;
        }
        Eigen::VectorXd xr = (1 + rho) * xbar - rho * sim.row(N).transpose();
        double fxr = cost_function(xr, xr, false);
        bool doshrink = false;

        if (fxr < fsim(0)) {
            Eigen::VectorXd xe = (1 + rho * chi) * xbar - rho* chi * sim.row(N).transpose();
            double fxe = cost_function(xe, xe, false);

            if (fxe < fxr) {
                sim.row(N) = xe;
                fsim(N) = fxe;
            }
            else {
                sim.row(N) = xr;
                fsim(N) = fxr;
            }
        }
        else {
            if (fxr < fsim(N - 1)) {
                sim.row(N) = xr;
                fsim(N) = fxr;
            }
            else {
                // Perform contraction
                if (fxr < fsim(N)) {
                    Eigen::VectorXd xc = (1 + psi * rho) *xbar - psi * rho * sim.row(N).transpose();
                    double fxc = cost_function(xc, xc, false);

                    if (fxc <= fxr) {
                        sim.row(N) = xc;
                        fsim(N) = fxc;
                    }
                    else {
                        doshrink = true;
                    }
                }
                else {
                    // Perform an inside contraction
                    Eigen::VectorXd xcc = (1 - psi) * xbar + psi * sim.row(N).transpose();
                    double fxcc = cost_function(xcc, xcc, false);

                    if (fxcc < fsim(N)) {
                        sim.row(N) = xcc;
                        fsim(N) = fxcc;
                    }
                    else {
                        doshrink = true;
                    }
                }

                if (doshrink)
                    for (int j = 1; j < N + 2; j++) {
                        sim.row(j) = sim.row(0) + sigma * (sim.row(j) - sim.row(0));
                        y = sim.row(j);
                        fsim(j) = cost_function(y, y, false);
                    }
            }
        }

        // Sort by increasing fsim value
        ind = argsort(fsim);
        for (int k = 0; k < N + 1; k++) {
            sim_sorted.row(k) = sim.row(ind[k]);
            fsim_sorted[k] = fsim[ind[k]];
        }
        sim = sim_sorted;
        fsim = fsim_sorted;

        iterations++;
    }

    MinimizationStatus status = MinimizationStatus::OK;
    if (cost_function.num_calls >= maxfev) {
        status = MinimizationStatus::TOO_MANY_COST_CUNCTION_CALLS;
    }
    else if (iterations >= maxiter) {
        status = MinimizationStatus::TOO_MANY_ITERATIONS;
    }

    MinimizationResult res;
    res.final_cost = fsim(0);
    res.final_x = sim.row(0);
    res.num_iterations = iterations;
    res.error_code = status;
    res.num_cost_function_calls = cost_function.num_calls;
    res.diff_cost = max_diff_f;
    res.diff_x = max_diff_x;
    return res;
}


bool startswith(std::string str, std::string prefix) {
    return str.compare(0, prefix.size(), prefix) == 0;
}

/*
    This subroutine computes a safeguarded step for a search
    procedure and updates an interval that contains a step that
    satisfies a sufficient decrease and a curvature condition.
    (subroutine DCSTEP from MINPACK)
*/
void dcstep(double &stx, double &fx, double &dx, double &sty, double &fy, double &dy, double &stp,
            const double fp, const double dp, bool &brackt, const double stpmin, const double stpmax) {
    double gamma, p, q, r, s, sgnd, stpc, stpf, stpq, theta;
    sgnd = dp * (dx / std::abs(dx));

    // First case: A higher function value. The minimum is bracketed.
    // If the cubic step is closer to stx than the quadratic step, the
    // cubic step is taken, otherwise the average of the cubic and
    // quadratic steps is taken.
    if (fp > fx) {
        theta = 3 * (fx - fp) / (stp - stx) + dx + dp;
        s = std::max(std::max(std::abs(theta), std::abs(dx)), std::abs(dp));
        gamma = s * std::sqrt(std::pow(theta / s, 2) - (dx / s) * (dp / s));
        if (stp < stx) gamma = -gamma;
        p = (gamma - dx) + theta;
        q = ((gamma - dx) + gamma) + dp;
        r = p / q;
        stpc = stx + r * (stp - stx);
        stpq = stx + ((dx / ((fx - fp) / (stp - stx) + dx)) / 2.0) * (stp - stx);
        if (std::abs(stpc - stx) < std::abs(stpq - stx))
            stpf = stpc;
        else
            stpf = stpc + (stpq - stpc) / 2.0;
         brackt = true;
    }
    // Second case: A lower function value and derivatives of opposite
    // sign. The minimum is bracketed. If the cubic step is farther from
    // stp than the secant step, the cubic step is taken, otherwise the
    // secant step is taken.
    else if (sgnd < 0) {
        theta = 3 * (fx - fp) / (stp - stx) + dx + dp;
        s = std::max(std::max(std::abs(theta), std::abs(dx)), std::abs(dp));
        gamma = s * std::sqrt(std::pow(theta / s, 2) - (dx / s) * (dp / s));
        if (stp > stx) gamma = -gamma;
        p = (gamma - dp) + theta;
        q = ((gamma - dp) + gamma) + dx;
        r = p / q;
        stpc = stp + r * (stx - stp);
        stpq = stp + (dp / (dp - dx)) * (stx - stp);
        if (std::abs(stpc - stp) > std::abs(stpq - stp))
            stpf = stpc;
        else
            stpf = stpq;
        brackt = true;
    }

    // Third case: A lower function value, derivatives of the same sign,
    // and the magnitude of the derivative decreases.
    else if (std::abs(dp) < std::abs(dx)) {
        // The cubic step is computed only if the cubic tends to infinity
        // in the direction of the step or if the minimum of the cubic
        // is beyond stp. Otherwise the cubic step is defined to be the
        // secant step.

        theta = 3 * (fx - fp) / (stp - stx) + dx + dp;
        s = std::max(std::max(std::abs(theta), std::abs(dx)), std::abs(dp));

        // The case gamma = 0 only arises if the cubic does not tend
        // to infinity in the direction of the step.
        gamma = s * std::sqrt(std::max(0.0, std::pow(theta / s, 2) - (dx / s) * (dp / s)));
        if (stp > stx) gamma = -gamma;
        p = (gamma - dp) + theta;
        q = (gamma + (dx - dp)) + gamma;
        r = p / q;
        if (r < 0 && gamma != 0)
            stpc = stp + r * (stx - stp);
        else if (stp > stx)
            stpc = stpmax;
        else
           stpc = stpmin;

        stpq = stp + (dp / (dp - dx)) * (stx - stp);

        if (brackt) {
            // A minimizer has been bracketed. If the cubic step is
            // closer to stp than the secant step, the cubic step is
            // taken, otherwise the secant step is taken.

            if (std::abs(stpc - stp) < std::abs(stpq - stp))
                stpf = stpc;
            else
                stpf = stpq;

            if (stp > stx)
                stpf = std::min(stp + 0.66 * (sty - stp), stpf);
            else
                stpf = std::max(stp + 0.66 * (sty - stp), stpf);
        }
        else {
            // A minimizer has not been bracketed. If the cubic step is
            // farther from stp than the secant step, the cubic step is
            // taken, otherwise the secant step is taken.
            if (std::abs(stpc - stp) > std::abs(stpq - stp))
                stpf = stpc;
            else
               stpf = stpq;
            stpf = std::min(stpmax, stpf);
            stpf = std::max(stpmin, stpf);
        }
    }

    // Fourth case: A lower function value, derivatives of the same sign,
    // and the magnitude of the derivative does not decrease. If the
    // minimum is not bracketed, the step is either stpmin or stpmax,
    // otherwise the cubic step is taken.
    else {
        if (brackt) {
            theta = 3 * (fp - fy) / (sty - stp) + dy + dp;
            s = std::max(std::max(std::abs(theta), std::abs(dy)), std::abs(dp));
            gamma = s * std::sqrt(std::pow(theta / s, 2) - (dy / s) * (dp / s));
            if (stp > sty) gamma = -gamma;
            p = (gamma - dp) + theta;
            q = ((gamma - dp) + gamma) + dy;
            r = p / q;
            stpc = stp + r * (sty - stp);
            stpf = stpc;
        }
        else if (stp > stx)
            stpf = stpmax;
        else
            stpf = stpmin;
    }

    // Update the interval which contains a minimizer.
    if (fp > fx) {
        sty = stp;
        fy = fp;
        dy = dp;
    }
    else {
        if (sgnd < 0) {
           sty = stx;
           fy = fx;
           dy = dx;
        }
        stx = stp;
        fx = fp;
        dx = dp;
    }

    // Compute the new step.
    stp = stpf;
}

/*
    This subroutine finds a step that satisfies a sufficient decrease condition and a
    curvature condition (subroutine DCSRCH from MINPACK)
*/
void dcsrch(double &stp, double &f, double &g, const double ftol, const double gtol,
            const double xtol, std::string &task, const double stpmin, const double stpmax,
            std::vector<int> &isave, std::vector<double> &dsave) {
    bool brackt;
    int stage;
    const double xtrapl = 1.1, xtrapu = 4.0;
    double finit, ftest, fm, fx, fxm, fy, fym, ginit, gtest, gm, gx, gxm, gy, gym, stx, sty,
            stmin, stmax, width, width1;

    // Helper closure to store out working variables to isave and dsave
    auto save = [&]() -> void {
        isave = {brackt, stage};
        dsave = {ginit, gtest, gx, gy, finit, fx, fy, stx, sty, stmin, stmax, width, width1};
    };

    if (startswith(task, "START")) {
        // Check the input arguments for errors and exit on any errors
        if (stp < stpmin) task = "ERROR: STP .LT. STPMIN";
        if (stp > stpmax) task = "ERROR: STP .GT. STPMAX";
        if (g >= 0) task = "ERROR: INITIAL G .GE. ZERO";
        if (ftol < 0) task = "ERROR: FTOL .LT. ZERO";
        if (gtol < 0) task = "ERROR: GTOL .LT. ZERO";
        if (xtol < 0) task = "ERROR: XTOL .LT. ZERO";
        if (stpmin < 0) task = "ERROR: STPMIN .LT. ZERO";
        if (stpmax < stpmin) task = "ERROR: STPMAX .LT. STPMIN";
        if (startswith(task, "ERROR")) return;

        // Initialize local variables
        brackt = false;
        stage = 1;
        finit = f;
        ginit = g;
        gtest = ftol * ginit;
        width = stpmax - stpmin;
        width1 = width / 0.5;

        // The variables stx, fx, gx contain the values of the step,
        // function, and derivative at the best step.
        // The variables sty, fy, gy contain the value of the step,
        // function, and derivative at sty.
        // The variables stp, f, g contain the values of the step,
        // function, and derivative at stp.
        stx = 0;
        fx = finit;
        gx = ginit;
        sty = 0;
        fy = finit;
        gy = ginit;
        stmin = 0;
        stmax = stp + xtrapu*stp;
        task = "FG";

        return save();
    }

    // Load saved variables
    brackt = isave[0];
    stage = isave[1];
    ginit = dsave[0];
    gtest = dsave[1];
    gx = dsave[2];
    gy = dsave[3];
    finit = dsave[4];
    fx = dsave[5];
    fy = dsave[6];
    stx = dsave[7];
    sty = dsave[8];
    stmin = dsave[9];
    stmax = dsave[10];
    width = dsave[11];
    width1 = dsave[12];

    // If psi(stp) <= 0 and f'(stp) >= 0 for some step, then the
    // algorithm enters the second stage.
    ftest = finit + stp * gtest;
    if (stage == 1 && f <= ftest && g >= 0) stage = 2;

    // Test for warnings.
    if (brackt && (stp <= stmin || stp >= stmax))
        task = "WARNING: ROUNDING ERRORS PREVENT PROGRESS";
    if (brackt && stmax - stmin <= xtol * stmax)
        task = "WARNING: XTOL TEST SATISFIED";
    if (stp == stpmax && f <= ftest && g <= gtest)
        task = "WARNING: STP = STPMAX";
    if (stp == stpmin && (f > ftest || g >= gtest))
        task = "WARNING: STP = STPMIN";

    // Test for convergence.
    if (f <= ftest && std::abs(g) <= gtol * (-ginit))
        task = "CONVERGENCE";

    // Test for termination.
    if (startswith(task, "WARN") || startswith(task, "CONV"))
        return save();

    // A modified function is used to predict the step during the
    // first stage if a lower function value has been obtained but
    // the decrease is not sufficient.
    if (stage == 1 && f <= fx && f > ftest) {
        // Define the modified function and derivative values.
        fm = f - stp * gtest;
        fxm = fx - stx * gtest;
        fym = fy - sty * gtest;
        gm = g - gtest;
        gxm = gx - gtest;
        gym = gy - gtest;

        // Call dcstep to update stx, sty, and to compute the new step.
        dcstep(stx, fxm, gxm, sty, fym, gym, stp, fm, gm, brackt, stmin, stmax);

        // Reset the function and derivative values for f.
        fx = fxm + stx * gtest;
        fy = fym + sty * gtest;
        gx = gxm + gtest;
        gy = gym + gtest;
    }
    else {
        // Call dcstep to update stx, sty, and to compute the new step.
        dcstep(stx, fx, gx, sty, fy, gy, stp, f, g, brackt, stmin, stmax);
    }

    // Decide if a bisection step is needed.
    if (brackt) {
        if (std::abs(sty - stx) >= 0.66 * width1)
            stp = stx + 0.5 * (sty - stx);
        width1 = width;
        width = abs(sty - stx);
    }

    // Set the minimum and maximum steps allowed for stp.
    if (brackt) {
       stmin = std::min(stx, sty);
       stmax = std::max(stx, sty);
    }
    else {
       stmin = stp + xtrapl * (stp - stx);
       stmax = stp + xtrapu * (stp - stx);
    }

    // Force the step to be within the bounds stpmax and stpmin.
    stp = std::max(stp, stpmin);
    stp = std::min(stp, stpmax);

    // If further progress is not possible, let stp be the best
    // point obtained during the search.
    if (brackt && (stp <= stmin || stp >= stmax) || (brackt && stmax - stmin <= xtol * stmax))
        stp = stx;

    // Obtain another function and derivative.
    task = "FG";
    return save();
}


/*
    Scalar function search for alpha that satisfies strong Wolfe conditions
    alpha > 0 is assumed to be a descent direction.

    phi : callable phi(alpha)
        Function at point `alpha`
    derphi : callable dphi(alpha)
        Derivative `d phi(alpha)/ds`. Returns a scalar.
    phi0 : float, optional
        Value of `f` at 0
    old_phi0 : float, optional
        Value of `f` at the previous point
    derphi0 : float, optional
        Value `derphi` at 0
    c1, c2 : float, optional
        Wolfe parameters
    amax, amin : float, optional
        Maximum and minimum step size
    xtol : float, optional
        Relative tolerance for an acceptable step.

    Returns

    alpha : float
        Step size, or None if no suitable step was found
    phi : float
        Value of `phi` at the new point `alpha`
    phi0 : float
        Value of `phi` at `alpha=0`
    line_search_ok: bool
*/
auto scalar_search_wolfe1(ScalarCostFunction &phi, double phi0, double old_phi0, double derphi0,
                          double amin=1e-8, double amax=50, double xtol=1e-14,
                          double c1=1e-4, double c2=0.9) {
    double alpha1 = 1.0;
    if (derphi0 != 0) {
        alpha1 = std::min(1.0, 1.01 * 2 * (phi0 - old_phi0) / derphi0);
        if (alpha1 < 0)
            alpha1 = 1.0;
    }

    double phi1 = phi0;
    double derphi1 = derphi0;
    auto isave = std::vector<int>(2);
    auto dsave = std::vector<double>(13);

    const std::size_t maxiter = 100;
    std::string task("START");
    bool converged = false;
    for (int i = 0; i < maxiter; i++) {
        dcsrch(alpha1, phi1, derphi1, c1, c2, xtol, task, amin, amax, isave, dsave);
        if (startswith(task, "FG")) {
            std::tie(phi1, derphi1) = phi(alpha1);
        }
        else {
            converged = true;
            break;
        }
    }
    if (startswith(task, "ERROR") || startswith(task, "WARN"))
        converged = false;

    return std::make_tuple(alpha1, phi1, phi0, converged);
}


/*
    Finds the minimizer for a cubic polynomial that goes through the
    points (a,fa), (b,fb), and (c,fc) with derivative at a of fpa.
    If no minimizer can be found return None
*/
auto _cubicmin(const double a, const double fa, const double fpa, const double b,
               const double fb, const double c, const double fc) {
    // f(x) = A *(x-a)^3 + B*(x-a)^2 + C*(x-a) + D
    double C = fpa;
    double db = b - a;
    double dc = c - a;
    double denom = std::pow(db * dc, 2) * (db - dc);
    if (denom == 0)
        return std::make_tuple(-1e100, false);

    double tmp0 = fb - fa - C * db;
    double tmp1 = fc - fa - C * dc;
    double A = (dc*dc*tmp0 - db*db*tmp1)/denom;
    double B = (-dc*dc*dc*tmp0 + db*db*db*tmp1)/denom;
    double radical = B * B - 3 * A * C;
    if (radical < 0)
        return std::make_tuple(-1e100, false);

    double xmin = a + (-B + std::sqrt(radical)) / (3 * A);
    if (!std::isfinite(xmin))
          return std::make_tuple(-1e100, false);

    return std::make_tuple(xmin, true);
}


/*
    Finds the minimizer for a quadratic polynomial that goes through
    the points (a,fa), (b,fb) with derivative at a of fpa,
*/
auto _quadmin(const double a, const double fa, const double fpa, const double b, const double fb) {
    // f(x) = B*(x-a)^2 + C*(x-a) + D
    double D = fa;
    double C = fpa;
    double db = b - a * 1.0;
    double B = (fb - D - C * db) / (db * db);
    if (B == 0)
        return std::make_tuple(-1e100, false);

    double xmin = a - C / (2.0 * B);
    if (!std::isfinite(xmin))
        return std::make_tuple(-1e100, false);

    return std::make_tuple(xmin, true);
}


// Part of the optimization algorithm in scalar_search_wolfe2.
auto _zoom(ScalarCostFunction &phi, double a_lo, double a_hi, double phi_lo, double phi_hi,
           double derphi_lo, double phi0, double derphi0, double c1, double c2) {
    double delta1 = 0.2;  // cubic interpolant check
    double delta2 = 0.1;  // quadratic interpolant check
    double phi_rec = phi0;
    double a_rec = 0;
    double cchk, a_j, phi_aj, derphi_aj;

    const std::size_t maxiter = 10;
    bool converged = false, min_ok;
    for (int i = 0; i < maxiter; i++) {
        // interpolate to find a trial step length between a_lo and
        // a_hi Need to choose interpolation here.  Use cubic
        // interpolation and then if the result is within delta *
        // dalpha or outside of the interval bounded by a_lo or a_hi
        // then use quadratic interpolation, if the result is still too
        // close, then use bisection
        double dalpha = a_hi - a_lo;
        double a, b;
        if (dalpha < 0) {
            a = a_hi;
            b = a_lo;
        }
        else {
            a = a_lo;
            b = a_hi;
        }

        // minimizer of cubic interpolant
        // (uses phi_lo, derphi_lo, phi_hi, and the most recent value of phi)
        //
        // if the result is too close to the end points (or out of the
        // interval) then use quadratic interpolation with phi_lo,
        // derphi_lo and phi_hi if the result is stil too close to the
        // end points (or out of the interval) then use bisection
        if (i > 0) {
            cchk = delta1 * dalpha;
            std::tie(a_j, min_ok) = _cubicmin(a_lo, phi_lo, derphi_lo, a_hi, phi_hi, a_rec, phi_rec);
        }
        if (i == 0 || !min_ok || a_j > b - cchk || a_j < a + cchk) {
            double qchk = delta2 * dalpha;
            std::tie(a_j, min_ok) = _quadmin(a_lo, phi_lo, derphi_lo, a_hi, phi_hi);
            if (!min_ok || (a_j > b - qchk) || (a_j < a + qchk))
                a_j = a_lo + 0.5*dalpha;
        }

        // Get value and derivative at a_j
        std::tie(phi_aj, derphi_aj) = phi(a_j);

        // Check new value of a_j
        if (phi_aj > phi0 + c1 * a_j * derphi0 || phi_aj >= phi_lo) {
            phi_rec = phi_hi;
            a_rec = a_hi;
            a_hi = a_j;
            phi_hi = phi_aj;
        }
        else {
            if (std::abs(derphi_aj) <= -c2 * derphi0) {
                converged = true;
                break;
            }
            if (derphi_aj * (a_hi - a_lo) >= 0) {
                phi_rec = phi_hi;
                a_rec = a_hi;
                a_hi = a_lo;
                phi_hi = phi_lo;
            }
            else {
                phi_rec = phi_lo;
                a_rec = a_lo;
            }
            a_lo = a_j;
            phi_lo = phi_aj;
            derphi_lo = derphi_aj;
        }
    }
    return std::make_tuple(a_j, phi_aj, derphi_aj, converged);
}


/*
    Scalar function search for alpha that satisfies strong Wolfe conditions
    alpha > 0 is assumed to be a descent direction.

    See scalar_search_wolfe1 or scipy.optimize.linesearch for more documentation
*/
auto scalar_search_wolfe2(ScalarCostFunction &phi, double phi0,
                          double old_phi0, double derphi0,
                          double amax=50, double xtol=1e-14,
                          double c1=1e-4, double c2=0.9) {
    double alpha0 = 0.0, alpha1 = 1.0, alpha_star, phi_star, derphi_star;
    if (derphi0 != 0)
        alpha1 = std::min(1.0, 1.01 * 2 * (phi0 - old_phi0) / derphi0);

    if (alpha1 < 0)
        alpha1 = 1.0;

    if (alpha1 == 0) {
        // This shouldn't happen. Perhaps the increment has slipped below
        // machine precision?  For now, set the return variables skip the
        // useless while loop, and raise warnflag=2 due to possible imprecision.
        phi_star = phi0;
        phi0 = old_phi0;
    }

    double phi_a1, derphi_a1;
    std::tie(phi_a1, derphi_a1) = phi(alpha1);

    double phi_a0 = phi0;
    double derphi_a0 = derphi0;

    const std::size_t maxiter = 10;
    bool converged = false;
    for (int i = 0; i < maxiter; i++) {
        if (alpha1 == 0) {
            converged = false;
            break;
        }

        // C++ porting comment to original code: should this be i >= 1 and not i > 0 ?
        // the original code looks like an incomplete rewrite from a while to a for loop
        if (phi_a1 > phi0 + c1 * alpha1 * derphi0 || (phi_a1 >= phi_a0 && i > 1)) {
            std::tie(alpha_star, phi_star, derphi_star, converged) =
                    _zoom(phi, alpha0, alpha1, phi_a0, phi_a1, derphi_a0, phi0, derphi0, c1, c2);
            break;
        }

        if (std::abs(derphi_a1) <= -c2*derphi0) {
            alpha_star = alpha1;
            phi_star = phi_a1;
            derphi_star = derphi_a1;
            converged = true;
            break;
        }

        if (derphi_a1 >= 0) {
            std::tie(alpha_star, phi_star, derphi_star, converged) =
                    _zoom(phi, alpha1, alpha0, phi_a1, phi_a0, derphi_a1, phi0, derphi0, c1, c2);
            break;
        }

        double alpha2 = 2 * alpha1;   // increase by factor of two on each iteration
        alpha0 = alpha1;
        alpha1 = alpha2;
        phi_a0 = phi_a1;
        derphi_a0 = derphi_a1;
        std::tie(phi_a1, derphi_a1) = phi(alpha1);
    }

    if (!converged) {
        // stopping test maxiter reached
        alpha_star = alpha1;
        phi_star = phi_a1;
    }

    return std::make_tuple(alpha_star, phi_star, phi0, converged);
}


/*
    Wrap a vector cost function for use with the scalar wolfe searches. The
    cost is the same, while the gradient is dotted with the pk vector. The
    original space is starting from xk, the 1D line space is oriented along
    pk with coordinate s as the distance from xk.
*/
struct VectorCostFunctionWrapper : public ScalarCostFunction {
    CostFunction *_cost_function;
    Eigen::VectorXd *_pk, *_xk;
    Eigen::VectorXd _gradient;
    VectorCostFunctionWrapper(CostFunction &cf, Eigen::VectorXd &xk, Eigen::VectorXd &pk){
        this->_cost_function = &cf;
        this->_xk = &xk;
        this->_pk = &pk;
        this->_gradient.resize(xk.rows(), 1);
    }
    std::tuple<double, double> operator()(const double s, bool need_gradient=true) override final {
        num_calls++;
        auto &pk = *(this->_pk);
        auto &cf = *(this->_cost_function);
        auto &xk = *(this->_xk);
        double cost = cf(xk + s*pk, this->_gradient, need_gradient);
        double gradient = 0;
        if (need_gradient)
            gradient = this->_gradient.transpose() * pk;
        return std::make_tuple(cost, gradient);
    }
};


/*
    Line search algorithms Wolfe1 and Wolfe2
    Documentation can be found in the scipy.minimize.linesearch module

    version == 1: Run Wolfe1
    version == 2: Run Wolfe2
    version == 12: Run Wolfe1 and if that failes then try Wolfe2
*/
template<int version>
auto line_search_wolfe(CostFunction &cost_function, Eigen::VectorXd &xk, Eigen::VectorXd &pk,
                       Eigen::VectorXd &gfk, double old_fval, double old_old_fval,
                       double amin=1e-8, double amax=50, double xtol=1e-14,
                       double c1=1e-4, double c2=0.9) {
    static_assert(version == 1 || version == 2 || version == 12, "Wolfe version must be 1, 2 or 12");

    // Wrap the vector coordinate cost function as a scalar cost function
    auto wcf = VectorCostFunctionWrapper(cost_function, xk, pk);

    double derphi0 = gfk.transpose() * pk;
    double stp;
    bool line_search_ok = true;

    std::tuple<double, double, double, bool> ret;
    if (version == 1 || version == 12) {
        // Run the Wolfe1 line search
        ret = scalar_search_wolfe1(wcf, old_fval, old_old_fval, derphi0, amin, amax, xtol, c1, c2);
        line_search_ok = std::get<bool>(ret);
    }
    if (version == 2 || (version == 12 && !line_search_ok)) {
        // Run the Wolfe2 line search, possibly as backup for Wolfe1 if it failed
        ret = scalar_search_wolfe2(wcf, old_fval, old_old_fval, derphi0, amax, xtol, c1, c2);
    }
    std::tie(stp, old_fval, old_old_fval, line_search_ok) = ret;

    Eigen::VectorXd gfkp1(wcf._gradient);
    return std::make_tuple(stp, old_fval, old_old_fval, gfkp1, line_search_ok);
}


/*
    Minimize a function using the quasi-Newton method of Broyden, Fletcher,
    Goldfarb, and Shanno (BFGS). See the documentation of scipy.optimize.minimize
    for more information. Reference: Wright, and Nocedal "Numerical Optimization",
    1999, page 198.

    cost_function : callable f(x, gradient, calc_gradient)
        Objective function to be minimized.
    x0 : ndarray
        Initial guess.
    maxiter : int, optional
        Maximum number of iterations to perform.
    gtol : float, optional
        Gradient norm must be less than gtol before successful termination.
*/
MinimizationResult fmin_bfgs(CostFunction &cost_function, const Eigen::VectorXd &x0,
                             std::size_t maxiter=0, const double gtol=1e-5) {
    const std::size_t N = x0.rows();
    if (maxiter == 0)
        maxiter = N * 200;

    Eigen::VectorXd gfk(N);
    std::size_t iterations = 0;
    auto I = Eigen::MatrixXd::Identity(N, N);
    Eigen::MatrixXd Hk(I), A1(N, N), A2(N, N), A3(N, N);
    Eigen::VectorXd xk(x0);

    double old_fval = cost_function(x0, gfk);
    double gnorm = gfk.norm();
    double old_old_fval = old_fval + gnorm/2;

    MinimizationStatus status = MinimizationStatus::OK;
    Eigen::VectorXd sk, pk, xkp1, gfkp1, yk;
    while (gnorm > gtol && iterations < maxiter) {
        iterations++;
        pk = -Hk*gfk;

        // Line search for improved solution
        auto line_search_res = line_search_wolfe<12>(cost_function, xk, pk, gfk,
                                                     old_fval, old_old_fval,
                                                     1e-100, 1e100);
        bool OK = std::get<4>(line_search_res);
        double alpha_k;
        if (OK) {
            // CHECKME: gradient_kp1 can be None in the Python code, verify that we always compute this!
            std::tie(alpha_k, old_fval, old_old_fval, gfkp1, OK) = line_search_res;
        }
        else {
            status = MinimizationStatus::LINE_SEARCH_FAILED_TO_IMPROVE_SOLUTION;
            break;
        }

        xkp1 = xk + alpha_k * pk;
        sk = xkp1 - xk;
        xk = xkp1;

        yk = gfkp1 - gfk;
        gfk = gfkp1;
        gnorm = gfk.norm();
        if (gnorm <= gtol) break;

        if (std::isinf(old_fval)) {
            status = MinimizationStatus::LINE_SEARCH_FAILED_TO_IMPROVE_SOLUTION;
            break;
        }

        double rhok;
        double dp = yk.transpose() * sk;
        if (dp != 0.0)
            rhok = 1.0 / dp;
        else
            rhok = 1000.0;
        if (std::isinf(rhok))
            rhok = 1000.0;

        for (int i = 0; i < N; i++) {
            A1.row(i) = I.row(i) - rhok * sk(i) * yk.transpose();
            A2.row(i) = I.row(i) - rhok * yk(i) * sk.transpose();
            A3.row(i) = rhok * sk(i) * sk.transpose();
        }
        Hk = A1 *(Hk * A2) + A3;
    }

    if (std::isnan(old_fval))
        status = MinimizationStatus::GOT_NAN_COST;
    if (iterations >= maxiter)
        status = MinimizationStatus::TOO_MANY_ITERATIONS;

    MinimizationResult res;
    res.final_cost = old_fval;
    res.final_x = xk;
    res.final_gradient = gfk;
    res.num_iterations = iterations;
    res.error_code = status;
    res.num_cost_function_calls = cost_function.num_calls;
    res.gradient_norm = gnorm;
    return res;
}


MinimizationResult minimize(CostFunction &cost_function, const Eigen::VectorXd &x0,
                            std::string method = "auto",
                            std::size_t maxiter=0, const double gtol=1e-5) {
    // Make an intelligent choice (same as chosen by scipy)
    if (method == "auto")
        method = "BFGS";

    if (method == "BFGS")
        return fmin_bfgs(cost_function, x0, maxiter);
    else if (method == "Nelder-Mead")
        return fmin_neldermead(cost_function, x0, maxiter);
    else
        throw "Minimization method '" + method + "' not implemented";
}

} // end namespace scipy_optimize

#endif // SCIPY_OPTIMIZE_H
